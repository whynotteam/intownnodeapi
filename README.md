# In Town Node API
- Author: Oguzhan Murat Cakmak

Feel free to click [this link](https://in-town-node-api.herokuapp.com/graphiql) to go live version.

#### Local Installation
1. Install Dependencies
```
yarn 
```
2. Add `.env` file and assign the corresponded variables. Ask omuratcakmak@gmail.com for more info.
```env
PORT=""
DATABASE_URL=""
JWT_KEY=""
```
3. To dev
```
yarn dev
```

#### Test 
Jest is used to test. Run
```
yarn test
```
There are prod and dev mongoDB databases. When we run test, we drop our db, and start with clean slate.

#### Development
You should use [MonHeaders](https://chrome.google.com/webstore/detail/modheader/idgpnmonknjnojddfkpgkljpfnnfcklj?hl=en) extension to run mutations and queries once you get your authorization jwt token.

![Car Image](modheader.png)

#### Deployment
1. Install `heroku-cli`
2. Login your heroku account using `heroku login`
3. Add remote `heroku` to your git
4. To deploy, `git push origin heroku master`

#### Nice to haves in the Future
- Redis
