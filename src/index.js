import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import cors from "cors";
import dotenv from "dotenv";
import express from "express";
import express_graphql from "express-graphql";
import mongoose from "mongoose";
import passport from "passport";

import { schema } from "./schema";
import initiatePassport from "./config/passport";
import root from "./resolvers";
import { verifyJwt } from "./utils";

const connectDB = () => {
  // connect remote database
  const db = process.env.DATABASE_URL;
  mongoose
    .connect(db)
    .then(() => console.log("Connected to MongoDB successfully"))
    .catch(e => console.error(e));
};

const asyncMiddleware = async (req, res, next) => {
  try {
    await verifyJwt(req);
    req.decoded = true;
    next();
  } catch (e) {
    req.decoded = false;
    next();
  }
};
const startServer = () => {
  // access env variables
  dotenv.config();

  connectDB();

  const app = express();

  // For each request, provide wildcard Access-Control-* headers via OPTIONS call
  app.use(cors());

  // For each request, parse request body into a JavaScript object where header Content-Type is application/json
  app.use(bodyParser.json());

  // For each request, parse cookies
  app.use(cookieParser());

  app.use(passport.initialize());
  initiatePassport(passport);

  app.use(asyncMiddleware);

  app.use(
    "/graphiql",
    express_graphql({
      schema,
      rootValue: root,
      graphiql: true,
    })
  );

  // start server
  const port = process.env.PORT || 5000;

  return app.listen(port, () =>
    console.log(`Server is running on port ${port}`)
  );
};

if (process.env.NODE_ENV !== "test") {
  startServer();
}

export default startServer;
