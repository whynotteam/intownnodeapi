export class AuthPayload {
  constructor({ email, token }) {
    this.email = email;
    this.token = token;
  }
}

// Future payloads go here...
