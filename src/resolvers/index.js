// Root resolver
import { login, register, modifyUser } from "./mutationResolvers";

const root = {
  message: "Hello Graphiql",
  // Decoded represents whether user is authorized
  login({ email, password }, { decoded }) {
    return login({ email, password, decoded });
  },
  register({ email, name, password }, { decoded }) {
    return register({ email, name, password, decoded });
  },
  // This resolver is active after user is authorized
  async modifyUser({ email, name }, { decoded }) {
    if (decoded) return modifyUser({ email, name, decoded });
    throw new Error("User is not authorized");
  },
};

export default root;
