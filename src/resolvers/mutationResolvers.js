import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";

import { User } from "../models";
import { AuthPayload } from "../payloads";

export async function login({ email, password, decoded }) {
  if (decoded) throw new Error("User already logged in");
  try {
    const user = await User.findOne({ email });
    if (!user) {
      throw new Error("This user does not exist");
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (isMatch) {
      const payload = { id: user.id, name: user.name };
      let token = jwt.sign(payload, process.env.JWT_KEY, { expiresIn: 3600 });
      const authPayload = new AuthPayload({
        email: email,
        token: "Bearer " + token,
      });
      return authPayload;
    } else {
      return { message: "This user does not exist" };
    }
  } catch (e) {
    throw e;
  }
}

export async function modifyUser({ email, name, decoded }) {
  if (!decoded) throw new Error("This user is not authorized");
  try {
    const user = await User.findOne({ email }, (error, doc) => {
      if (email) {
        doc.email = email;
      }
      if (name) {
        doc.name = name;
      }
      doc
        .save()
        .then(user => {
          return user;
        })
        .catch(e => {
          throw new Error(e.message);
        });
    });
    if (user) {
      return { name: user.name, email: user.email };
    }
  } catch (e) {
    throw e;
  }
}

export async function register({ email, name, password, decoded }) {
  if (decoded) throw new Error("User already logged in");
  const user = await User.findOne({ email: email });
  if (user) {
    throw new Error("A user has already registered with this address");
  }
  const newUser = new User({
    name: name,
    email: email,
    password: password,
  });
  let salt;
  try {
    salt = await bcrypt.genSalt(10);
  } catch (e) {
    throw new Error(e.message);
  }
  let hash;
  try {
    hash = await bcrypt.hash(newUser.password, salt);
  } catch (e) {
    throw new Error(e.message);
  }
  newUser.password = hash;

  const authPayload = newUser
    .save()
    .then(user => {
      const payload = { id: user.id, name: user.name };
      const token = jwt.sign(payload, process.env.JWT_KEY, {
        expiresIn: 3600,
      });
      return new AuthPayload({
        email: email,
        token: "Bearer " + token,
      });
    })
    .catch(err => {
      throw new Error(err.message);
    });

  return authPayload;
}
