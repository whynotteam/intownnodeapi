// GraphQL schema
import { buildSchema } from "graphql";

/*
 * Since I am looking for performance and efficiency, I use pure graphql with no dependencies such as Apollo.
 * This is a trade-off between machine time vs human time. The downside is huge strings with no checking.
 */
export const schema = buildSchema(`
  type AuthPayload {
    email: String
    token: String
  }
  type ModifyPayload {
    email: String
    name: String
  }
  type Query {
    message: String!
  }
  type Mutation {
    login(email: String!, password: String!): AuthPayload
    register(email: String!, name: String!, password: String!): AuthPayload
    modifyUser(email: String, name: String): ModifyPayload
  }
`);
