const jwt = require("jsonwebtoken");

export const signToken = str => {
  return new Promise(resolve => {
    resolve(jwt.sign({ apiKey: str }, process.env.JWT_KEY));
  });
};

// To check whether user authorized
export const verifyJwt = req => {
  let token;
  if (req.query && req.query.hasOwnProperty("access_token")) {
    token = req.query.access_token;
  } else if (
    req.headers &&
    req.headers.authorization &&
    req.headers.authorization.includes("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }
  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_KEY, (error, decoded) => {
      if (error) reject("401: User is not authenticated");
      resolve(decoded);
    });
  });
};
