import request from "supertest";
import { request as gqRequest } from "graphql-request";
import mongoose from "mongoose";

import startServer from "../src/index.js";

let host = "",
  server;
const email = "oguzhanmc@gmail.com";
const name = "Oguzhan";
const password = "1234567890";

const mutation = `mutation {
  register(email: "${email}", name: "${name}", password: "${password}"){
    token
    email
  }
}`;

const loginMutation = `mutation {
  login(email: "${email}", password: "${password}"){
    token
    email
  }
}`;

afterAll(async () => {
  server.close();
});

beforeAll(async () => {
  server = await startServer();
  console.log(server);
  // Droping the database before all to have clean slate
  mongoose.connection.dropDatabase();
  host = `http://127.0.0.1:${process.env.PORT}`;
});

describe("SIGN UP", () => {
  it("should register user properly", async () => {
    const { register } = await gqRequest(host + "/graphiql", mutation);
    expect(register.token).toBeDefined();
    expect(register.email).toBe(email);
  });
  it("should fail with existing user", async () => {
    try {
      await gqRequest(host + "/graphiql", mutation);
    } catch (e) {
      expect(e.response.errors[0].message).toBe(
        "A user has already registered with this address"
      );
    }
  });
});

describe("LOGIN", () => {
  it("should login user properly", async () => {
    const { login } = await gqRequest(host + "/graphiql", loginMutation);
    expect(login.token).toBeDefined();
    expect(login.email).toBe(email);
  });
});

describe("GET graphiql", () => {
  it("should render graphiql", async () => {
    await request(host)
      .get("/graphiql")
      .query("");
  });
});

describe("GET /404", () => {
  it("should return 404 for non-existent URLs", async () => {
    await request(host)
      .get("/404")
      .expect(404);
    await request(host)
      .get("/notfound")
      .expect(404);
  });
});
